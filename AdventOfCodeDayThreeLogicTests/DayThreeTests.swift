//
//  DayThreeTests.swift
//  AdventOfCodeDayThree
//
//  Created by Anna Fröblom on 31/01/16.
//  Copyright © 2016 Anna Fröblom. All rights reserved.
//

import Quick
import Nimble
@testable import AdventOfCodeDayThreeLogic

class DayThreeSpec: QuickSpec {
    override func spec() {
        var counter: HouseVisitCounter!
        beforeEach {
            counter = HouseVisitCounter()
        }
        
        describe("countUniqueVisits") {
            it("returns 1 when input is empty") {
                expect(counter.countUniqueVisits(fromInput: "")).to(equal(1))
            }
            
            it("returns 2 when only one direction is given") {
                expect(counter.countUniqueVisits(fromInput: ">")).to(equal(2))
            }
            
            it("returns 4 from ^>v<") {
                expect(counter.countUniqueVisits(fromInput: "^>v<")).to(equal(4))
            }
            
            it("returns 2 from ^v^v^v^v^v") {
                expect(counter.countUniqueVisits(fromInput: "^v^v^v^v^v")).to(equal(2))
            }
        }
        
        describe("countUniqueVisitsRoboMode") {
            it("returns 1 when input is empty") {
                expect(counter.countUniqueVisitsRoboMode(fromInput: "")).to(equal(1))
            }
            
            it("returns 2 when only one direction is given") {
                expect(counter.countUniqueVisitsRoboMode(fromInput: ">")).to(equal(2))
            }
            
            it("returns 3 from ^v") {
                expect(counter.countUniqueVisitsRoboMode(fromInput: "^v")).to(equal(3))
            }
            
            it("returns 3 from ^>v<") {
                expect(counter.countUniqueVisitsRoboMode(fromInput: "^>v<")).to(equal(3))
            }
            
            it("returns 11 from ^v^v^v^v^v") {
                expect(counter.countUniqueVisitsRoboMode(fromInput: "^v^v^v^v^v")).to(equal(11))
            }
        }
    }
}
