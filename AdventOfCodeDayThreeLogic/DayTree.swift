//
//  DayTree.swift
//  AdventOfCodeDayThree
//
//  Created by Anna Fröblom on 31/01/16.
//  Copyright © 2016 Anna Fröblom. All rights reserved.
//

import Foundation

internal enum Direction: Character {
    case North = "^"
    case South = "v"
    case West = "<"
    case East = ">"
}

internal struct Location {
    internal let x: Int
    internal let y: Int
}

extension Location: Hashable {
    var hashValue: Int {
        return x ^ y
    }
}

extension Location: Equatable {}

func ==(lhs: Location, rhs: Location) -> Bool {
    return lhs.x == rhs.x && lhs.y == rhs.y
}

public class HouseVisitCounter {
    public init() {}
    
    public func countUniqueVisits(fromInput input: String) -> Int {
        let directions = input.characters.map { Direction(rawValue: $0)! }
        return visit(directions).count
    }
    
    public func countUniqueVisitsRoboMode(fromInput input: String) -> Int {
        let directions = input.characters.map { Direction(rawValue: $0)! }
        let directionsWithIndex = directions.enumerate()
        let santasDirections = directionsWithIndex.filter { $0.index % 2 != 0 }.map { $0.element }
        let roboSantasDirections = directionsWithIndex.filter { $0.index % 2 == 0 }.map { $0.element }
        let santasVisits = visit(santasDirections)
        let roboSantasVisit = visit(roboSantasDirections)
        return santasVisits.union(roboSantasVisit).count
    }
    
    private func visit(directions: [Direction]) -> Set<Location> {
        var visitedLocations: Set<Location> = [Location(x: 0, y: 0)]
        var currentLocation = visitedLocations.first!
        
        for direction in directions {
            let newLocation = moveTo(direction, fromLocation: currentLocation)
            visitedLocations.insert(newLocation)
            currentLocation = newLocation
        }
        return visitedLocations
    }
    
    private func moveTo(direction: Direction, fromLocation location: Location) -> Location {
        var x = location.x, y = location.y
        switch direction {
            case .North:
                y++
            case .South:
                y--
            case .West:
                x--
            case .East:
                x++
        }
        return Location(x: x, y: y)
    }
}

